import sys
import RPi.GPIO as GPIO
import urllib2
import httplib
import urllib
import time
from time import sleep
key = "0UR4X4I456HMFK3I"

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#set GPIO direction (IN/OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def distance():
        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER, True)

        # set Trigger after 7s to LOW
        time.sleep(7)
        GPIO.output(GPIO_TRIGGER, False)

        StartTime = time.time()
        StopTime = time.time()

        # save StartTime
        while GPIO.input(GPIO_ECHO) == 0:
                StartTime = time.time()

        # save time of arrival
        while GPIO.input(GPIO_ECHO) == 1:
                StopTime = time.time()

        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with sonic speed (34300 cm/s)
        # and divide by 2 since there and back
        distance = (TimeElapsed * 34300) / 2

        return distance

def status_update():
	while True:
		dist=distance()	
		params = urllib.urlencode({'field1': dist, 'key':key })
		headers = {"Content-typZZe": "application/x-www-form-urlencoded","Accept": "text/plain"}
		conn = httplib.HTTPConnection("api.thingspeak.com:80")
		try:
			conn.request("POST", "/update", params, headers)
			response = conn.getresponse()
			print ("Distance =",dist)
			print response.status,response.reason
			data = distance()
			conn.close()
		except:
			print "connection failed"
		break
if __name__ == "__main__":
	while True:
		status_update()
	
